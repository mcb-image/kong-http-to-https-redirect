package = "kong-http-to-https-redirect"
version = "1.0.3-0"
source = {
    url = "git@gitlab.com:mcb-image/kong-http-to-https-redirect.git",
    branch = "master"
}
description = {
    summary = "A Kong plugin for redirecting HTTP traffic to HTTPS. Taken from https://github.com/HappyValleyIO/kong-http-to-https-redirect",
    detailed = [[
      Redirects traffic from HTTP to HTTPS (currently only offers 301 redirect).
    ]],
    homepage = "https://gitlab.com/mcb-image/kong-http-to-https-redirect",
    license = "MIT"
}
dependencies = {
}
build = {
    type = "builtin",
    modules = {
    ["kong.plugins.kong-http-to-https-redirect.handler"] = "src/handler.lua",
    ["kong.plugins.kong-http-to-https-redirect.schema"] = "src/schema.lua",
    }
}
